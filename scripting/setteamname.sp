#include <sourcemod> 
#include <smlib>

new Handle:g_hTeamOne = INVALID_HANDLE;
new Handle:g_hTeamTwo = INVALID_HANDLE;

public Plugin:myinfo =
{
    name = "Set Team Name",
    author = "Bubka3",
    description = "Sets team name based on CVar.",
    version = "1.2.0",
    url = "http://www.bubka3.com/"
};

public OnPluginStart()
{
    g_hTeamOne = CreateConVar("sm_teamname_one", "FirstTeam", "Set your first team name. (HL2DM: Combine | CSS: Terrorists)", FCVAR_PLUGIN);
    g_hTeamTwo = CreateConVar("sm_teamname_two", "SecondTeam", "Set your second team name. (HL2DM: Rebel | CSS: Counter-Terrorists)", FCVAR_PLUGIN);
}

public OnMapStart()
{
	decl String:sBuffer[MAX_TEAM_NAME_LENGTH];
	GetConVarString(g_hTeamOne, sBuffer, sizeof(sBuffer));
	Team_SetName(TEAM_ONE, sBuffer);
	GetConVarString(g_hTeamTwo, sBuffer, sizeof(sBuffer));
	Team_SetName(TEAM_TWO, sBuffer);
}