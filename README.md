# Public Plugins for SourceMod

This repository contains all of the plugins that I have made and [publicly released](http://www.sourcemod.net/plugins.php?cat=0&mod=-1&title=&author=Bubka3&description=&search=1) for SourceMod.

* ✔ [New IP Redirect](https://forums.alliedmods.net/showthread.php?t=153769)
* ✔ [Set Game Server Account](https://forums.alliedmods.net/showthread.php?t=237481)
* ✔ [Set Team Name](https://forums.alliedmods.net/showthread.php?t=153778)

Plugins approved by SourceMod are marked with ✔.